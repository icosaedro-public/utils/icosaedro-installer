#!/bin/bash

CURRENT_USER=$(whoami)
GITLAB_SERVER="gitlab.renanet.es:4443"

if [ "${CURRENT_USER}" != "root" ]; then
  echo "Please run this script as root or with sudo"
  exit
fi

echo "Updating system..."
apt-get update -y

echo "Preparing base system..."
apt-get -y install net-tools python3 git

echo -n "Please, enter GIT repository user: "
read -r GIT_USER
echo -n "
Please, enter GIT repository pass: "
read -r -s GIT_PASS
echo ""
GIT_PASS_URL=$(python3 -c "import sys, urllib.parse as ul; print(ul.quote_plus('${GIT_PASS}'));")

echo "Preparing system for Icosaedro Generic OpenVPN..."
apt-get -y install openvpn

echo "Downloading Generic OpenVPN certificates..."
ICOSAEDRO_OVPN_SERVER_ID=icosaedro-vpn-server
ICOSAEDRO_OVPN_CERT_LOCATION="/etc/openvpn"
ICOSAEDRO_OVPN_CERT_TMP_DIR=$(mktemp -d)
export GIT_SSL_NO_VERIFY=1 && git clone "https://${GIT_USER}:${GIT_PASS_URL}@${GITLAB_SERVER}/icosaedro/utils/icosaedro-openvpn.git" "${ICOSAEDRO_OVPN_CERT_TMP_DIR}"

echo "Configuring OpenVPN ${ICOSAEDRO_OVPN_SERVER_ID} server..."
cp -r "${ICOSAEDRO_OVPN_CERT_TMP_DIR}/openvpn-generic/server/"* "${ICOSAEDRO_OVPN_CERT_LOCATION}/server"
cp -r "${ICOSAEDRO_OVPN_CERT_TMP_DIR}/openvpn-generic/ips" "${ICOSAEDRO_OVPN_CERT_LOCATION}"
cp "${ICOSAEDRO_OVPN_CERT_TMP_DIR}/openvpn-generic/${ICOSAEDRO_OVPN_SERVER_ID}.conf" "${ICOSAEDRO_OVPN_CERT_LOCATION}"
rm -rf "${ICOSAEDRO_OVPN_CERT_TMP_DIR}"
ln -s /var/log/openvpn/ ${ICOSAEDRO_OVPN_CERT_LOCATION}/logs

echo "Enabling OpenVPN ${ICOSAEDRO_OVPN_SERVER_ID} server..."
systemctl enable openvpn@${ICOSAEDRO_OVPN_SERVER_ID}
echo "Starting OpenVPN ${ICOSAEDRO_OVPN_SERVER_ID} server..."
systemctl start openvpn@${ICOSAEDRO_OVPN_SERVER_ID}

echo "Work done!"
