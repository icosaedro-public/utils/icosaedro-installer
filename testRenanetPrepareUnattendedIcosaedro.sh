#!/bin/bash

CURRENT_USER=$(whoami)
NEXUS_SERVER="docker-ico-dev.renanet.es"
GITLAB_SERVER="gitlab.renanet.es"
BROKER_SERVERS="broker.renanet.es:9093"
DOCKER_SERVICES_HOME=/srv/DockerServices
DOCKER_SERVICES_CERT_LOCATION="/DockerServices/Docker/icosaedro-common/certs/broker"
DOCKER_SERVICES_CERT_KS_LOCATION="/keystore/icosaedro_hotel_client.keystore.jks"
DOCKER_SERVICES_CERT_TS_LOCATION="/truststore/broker.truststore.jks"
ICOSAEDRO_UPDATER_INITIAL_VERSION="1.0.0"
ICOSAEDRO_UPDATER_ENDPOINT="/icosaedro-public/icosaedro-updater-update/-/raw/master/${ICOSAEDRO_UPDATER_INITIAL_VERSION}/icosaedro-updater.jar?inline=false"
ICOSAEDRO_UPDATER_BIN="icosaedro-updater.jar"
ICOSAEDRO_UPDATER_LOCATION="${DOCKER_SERVICES_HOME}/application"
ICOSAEDRO_PROJECT_LOCATION="${DOCKER_SERVICES_HOME}/Icosaedro"

if [ "${CURRENT_USER}" != "root" ]; then
  echo "Please run this script as root or with sudo"
  exit
fi

if [ $# -lt 1 ]; then
  echo "Please tell me which user will be the docker manger: $0 userName"
  exit
fi

DOCKER_USER=$1

echo "Updating system..."
apt-get update -y

echo "Preparing base system..."
apt-get -y install net-tools python3 git

echo "Credentials located at: https://pass.renanet.es/app/passwords/view/e855015e-a0ce-425a-a805-c842f1273c58"
echo -n "Please, enter GIT repository user: "
read -r GIT_USER
echo -n "
Please, enter GIT repository pass: "
read -r -s GIT_PASS
echo ""
GIT_PASS_URL=$(python3 -c "import sys, urllib.parse as ul; print(ul.quote_plus('${GIT_PASS}'));")

echo "Preparing system for Icosaedro OpenVPN..."
apt-get -y install openvpn

echo "Downloading OpenVPN Ubuntu 22+ certificates..."
ICOSAEDRO_OVPN_SERVER_ID=icosaedro-hotspot-server
ICOSAEDRO_OVPN_VERSION="openvpn-22"
ICOSAEDRO_OVPN_CERT_LOCATION="/etc/openvpn"
ICOSAEDRO_OVPN_CERT_TMP_DIR=$(mktemp -d)
export GIT_SSL_NO_VERIFY=1 && git clone "https://${GIT_USER}:${GIT_PASS_URL}@${GITLAB_SERVER}/icosaedro/utils/icosaedro-openvpn.git" "${ICOSAEDRO_OVPN_CERT_TMP_DIR}"

echo "Configuring OpenVPN ${ICOSAEDRO_OVPN_SERVER_ID} server..."
cp -r "${ICOSAEDRO_OVPN_CERT_TMP_DIR}/${ICOSAEDRO_OVPN_VERSION}/server/"* "${ICOSAEDRO_OVPN_CERT_LOCATION}/server"
cp -r "${ICOSAEDRO_OVPN_CERT_TMP_DIR}/${ICOSAEDRO_OVPN_VERSION}/ips" "${ICOSAEDRO_OVPN_CERT_LOCATION}"
cp "${ICOSAEDRO_OVPN_CERT_TMP_DIR}/${ICOSAEDRO_OVPN_VERSION}/${ICOSAEDRO_OVPN_SERVER_ID}.conf" "${ICOSAEDRO_OVPN_CERT_LOCATION}"
rm -rf "${ICOSAEDRO_OVPN_CERT_TMP_DIR}"
ln -s /var/log/openvpn/ ${ICOSAEDRO_OVPN_CERT_LOCATION}/logs

echo "Enabling OpenVPN ${ICOSAEDRO_OVPN_SERVER_ID} server..."
systemctl enable openvpn@${ICOSAEDRO_OVPN_SERVER_ID}
echo "Starting OpenVPN ${ICOSAEDRO_OVPN_SERVER_ID} server..."
systemctl start openvpn@${ICOSAEDRO_OVPN_SERVER_ID}

echo "Preparing system for Icosaedro auto-updater..."
apt-get -y install openjdk-17-jdk uuid-runtime
mkdir -p "${DOCKER_SERVICES_HOME}"
mkdir -p "${ICOSAEDRO_UPDATER_LOCATION}/${ICOSAEDRO_UPDATER_INITIAL_VERSION}"
mkdir -p "${ICOSAEDRO_PROJECT_LOCATION}"

ln -s "${ICOSAEDRO_UPDATER_LOCATION}/${ICOSAEDRO_UPDATER_INITIAL_VERSION}" "${ICOSAEDRO_UPDATER_LOCATION}/latest"

echo "Downloading broker certificates..."
ICOSAEDRO_BROKER_CERT_LOCATION="${ICOSAEDRO_UPDATER_LOCATION}/ssl/broker-certs"
ICOSAEDRO_BROKER_CERT_KS_LOCATION="${ICOSAEDRO_BROKER_CERT_LOCATION}${DOCKER_SERVICES_CERT_KS_LOCATION}"
ICOSAEDRO_BROKER_CERT_TS_LOCATION="${ICOSAEDRO_BROKER_CERT_LOCATION}${DOCKER_SERVICES_CERT_TS_LOCATION}"
mkdir -p "$(dirname "${ICOSAEDRO_BROKER_CERT_KS_LOCATION}")"
mkdir -p "$(dirname "${ICOSAEDRO_BROKER_CERT_TS_LOCATION}")"

ICOSAEDRO_BROKER_CERT_TMP_DIR=$(mktemp -d)
export GIT_SSL_NO_VERIFY=1 && git clone "https://${GIT_USER}:${GIT_PASS_URL}@${GITLAB_SERVER}/icosaedro/utils/docker-services.git" "${ICOSAEDRO_BROKER_CERT_TMP_DIR}"
cp "${ICOSAEDRO_BROKER_CERT_TMP_DIR}${DOCKER_SERVICES_CERT_LOCATION}${DOCKER_SERVICES_CERT_KS_LOCATION}" "${ICOSAEDRO_BROKER_CERT_KS_LOCATION}"
cp "${ICOSAEDRO_BROKER_CERT_TMP_DIR}${DOCKER_SERVICES_CERT_LOCATION}${DOCKER_SERVICES_CERT_TS_LOCATION}" "${ICOSAEDRO_BROKER_CERT_TS_LOCATION}"
rm -rf "${ICOSAEDRO_BROKER_CERT_TMP_DIR}"

ICOSAEDRO_UPDATER_CONFIG_FILE=${ICOSAEDRO_UPDATER_LOCATION}/application-icosaedro.yml
if [ ! -f "${ICOSAEDRO_UPDATER_CONFIG_FILE}" ]; then
  SERVER_CODE=$(uuidgen)
  echo -n "Please, enter Broker SSL key password: "
  read -r -s BROKER_SSL_KEY_PASS
  echo "
logging:
  level:
    root: warn
    com.icosaedro: info
icosaedro:
  broker:
    address: ${BROKER_SERVERS}
    code: ${SERVER_CODE}
    ssl:
      security-protocol: SSL
      key-password: ${BROKER_SSL_KEY_PASS}
      keystore-location: ${ICOSAEDRO_BROKER_CERT_KS_LOCATION}
      keystore-password: ${BROKER_SSL_KEY_PASS}
      truststore-location: ${ICOSAEDRO_BROKER_CERT_TS_LOCATION}
      truststore-password: ${BROKER_SSL_KEY_PASS}
  updater:
    git:
      user: ${GIT_USER}
      password: ${GIT_PASS}
" > "${ICOSAEDRO_UPDATER_CONFIG_FILE}"
fi

cd "${ICOSAEDRO_UPDATER_LOCATION}/${ICOSAEDRO_UPDATER_INITIAL_VERSION}" || (echo "Unable to relocate at [${ICOSAEDRO_UPDATER_LOCATION}]" && exit)
wget --no-check-certificate -O "${ICOSAEDRO_UPDATER_BIN}" "https://${GITLAB_SERVER}${ICOSAEDRO_UPDATER_ENDPOINT}"

chown -R "${DOCKER_USER}:${DOCKER_USER}" ${DOCKER_SERVICES_HOME}

echo "Preparing system for docker..."
apt-get -y install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update -y

echo "Installing Docker dependencies..."
apt-get -y install ca-certificates curl

echo "Adding Docker's official GPG key:..."
install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
chmod a+r /etc/apt/keyrings/docker.asc

if [ ! -f /etc/apt/sources.list.d/docker.list ]; then
  echo "Adding the Docker repository to APT sources..."
  echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
    $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
    tee /etc/apt/sources.list.d/docker.list > /dev/null
fi

echo "Removing obsolete dependencies..."
for pkg in docker.io docker-doc docker-compose docker-compose-v2 podman-docker containerd runc docker-scan-plugin; do apt-get -y remove $pkg; done

echo "Installing Docker..."
apt-get -y install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

if [ "$(getent passwd "${DOCKER_USER}" | wc -l)" -eq 0 ]; then
  echo "User ${DOCKER_USER} does not exists, please insert user account info"
  adduser "${DOCKER_USER}"
fi

if [ "$(getent group docker | grep -c "${DOCKER_USER}")" -eq 0 ]; then
  echo "Adding user ${DOCKER_USER} to docker group"
  usermod -aG docker "${DOCKER_USER}"
  systemctl restart docker
fi

if [ -f /etc/docker/daemon.json ]; then
  rm /etc/docker/daemon.json
fi

echo "{
  \"log-driver\": \"json-file\",
  \"log-opts\": {
    \"max-size\": \"10m\",
    \"max-file\": \"10\",
    \"compress\": \"true\"
  }
}" >> /etc/docker/daemon.json
systemctl restart docker


DOCKER_USER_HOME=$(getent passwd "${DOCKER_USER}" | cut -d: -f6)
if [ ! -f "${DOCKER_USER_HOME}/.docker/config.json" ] || [ "$(grep -c ${NEXUS_SERVER} "${DOCKER_USER_HOME}/.docker/config.json")" -eq 0 ]; then
    echo "Nexus credentials located at: https://pass.renanet.es/app/passwords/view/ad4d97f1-4421-48aa-9f44-be3af664a001"
    echo "Please, login with user and password to NEXUS repository: ${NEXUS_SERVER}"
    runuser -l "${DOCKER_USER}" -c "docker login ${NEXUS_SERVER}"
fi

if [ "$(systemctl is-enabled docker)" != "enabled" ]; then
  echo "Enabling docker service on start..."
  systemctl enable docker
fi

echo "Verify docker version"
runuser -l "${DOCKER_USER}" -c "docker -v"

echo "DOCKER Installation Done"

ICOSAEDRO_UPDATER_WATCHDOG_NAME="icosaedroUpdaterWatchdog.sh"
if [ "$(crontab -l | grep -c "${ICOSAEDRO_UPDATER_WATCHDOG_NAME}")" -eq 0 ]; then
  echo "Enabling auto-updater watchdog..."

  ICOSAEDRO_UPDATER_WATCHDOG_FILE="${ICOSAEDRO_UPDATER_LOCATION}/${ICOSAEDRO_UPDATER_WATCHDOG_NAME}"
  echo '#!/bin/bash'"

# Ensure paths
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

if [ \"\$(ps -Af | grep \"${ICOSAEDRO_UPDATER_BIN}\" | grep -v grep -c)\" -eq 0 ]; then
  cd \"${ICOSAEDRO_UPDATER_LOCATION}\" && nohup java -Djdk.lang.Process.launchMechanism=vfork -jar latest/icosaedro-updater.jar > /dev/null 2>&1 &
fi
" > ${ICOSAEDRO_UPDATER_WATCHDOG_FILE}
  chmod +x ${ICOSAEDRO_UPDATER_WATCHDOG_FILE}
  chown -R "${DOCKER_USER}:${DOCKER_USER}" ${ICOSAEDRO_UPDATER_WATCHDOG_FILE}

  ICOSAEDRO_UPDATER_CRONTAB_CMD="* * * * * /bin/bash ${ICOSAEDRO_UPDATER_WATCHDOG_FILE}"
  runuser -l "${DOCKER_USER}" -c "(crontab -l; echo ${ICOSAEDRO_UPDATER_CRONTAB_CMD}) | crontab -"
fi

echo "Please re-login to allow user to use docker commands and configure environment at ${INSTALL_LOCATION} before launch Icosaedro system. Have a nice day."

