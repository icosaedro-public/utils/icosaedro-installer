#!/bin/bash

CURRENT_USER=$(whoami)
NEXUS_SERVER="docker-ico-prod.renanet.es"
GITLAB_SERVER="gitlab.renanet.es"
DOCKER_SERVICES_HOME=/srv/DockerServices
ICOSAEDRO_PROJECT_LOCATION="${DOCKER_SERVICES_HOME}/Icosaedro"

if [ "${CURRENT_USER}" != "root" ]; then
  echo "Please run this script as root or with sudo"
  exit
fi

DOCKER_USER=$1

echo "Updating system..."
apt-get update -y

echo "Preparing base system..."
apt-get -y install net-tools python3 git

echo "Preparing system for docker..."
apt-get -y install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update -y

echo "Installing Docker dependencies..."
apt-get -y install ca-certificates curl

echo "Adding Docker's official GPG key:..."
install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
chmod a+r /etc/apt/keyrings/docker.asc

if [ ! -f /etc/apt/sources.list.d/docker.list ]; then
  echo "Adding the Docker repository to APT sources..."
  echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
    $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
    tee /etc/apt/sources.list.d/docker.list > /dev/null
fi

echo "Removing obsolete dependencies..."
for pkg in docker.io docker-doc docker-compose docker-compose-v2 podman-docker containerd runc docker-scan-plugin; do apt-get -y remove $pkg; done

echo "Installing Docker..."
apt-get -y install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

if [ -f /etc/docker/daemon.json ]; then
  rm /etc/docker/daemon.json
fi

echo "{
  \"log-driver\": \"json-file\",
  \"log-opts\": {
    \"max-size\": \"10m\",
    \"max-file\": \"10\",
    \"compress\": \"true\"
  }
}" >> /etc/docker/daemon.json


echo "User your LDAP credentials for your PC"
echo "OR for servers user Nexus credentials located at: https://pass.renanet.es/app/passwords/view/ad4d97f1-4421-48aa-9f44-be3af664a001"
echo "Please, login with user and password to NEXUS repository: ${NEXUS_SERVER}"
runuser -l "${DOCKER_USER}" -c "docker login ${NEXUS_SERVER}"

systemctl restart docker
