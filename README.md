# Icosaedro Installer

Scripts to create an Icosaedro installation

The script can be run with the command:

```
wget --no-check-certificate -O /tmp/installIco.sh https://gitlab.renanet.es/icosaedro-public/utils/icosaedro-installer/raw/master/prepareIcosaedroV26Plus.sh?inline=false \
&& chmod +x /tmp/installIco.sh \
&& sudo bash -c "/tmp/installIco.sh $USER" \
&& rm /tmp/installIco.sh
```

For unattended updater script, use the command:

```
wget --no-check-certificate -O /tmp/installIco.sh https://gitlab.renanet.es/icosaedro-public/utils/icosaedro-installer/raw/master/prepareUnattededIcosaedroV26Plus.sh?inline=false \
&& chmod +x /tmp/installIco.sh \
&& sudo bash -c "/tmp/installIco.sh $USER" \
&& rm /tmp/installIco.sh
```

For stand-alone generic OpenVPN server installation, use the command:

```
wget --no-check-certificate -O /tmp/installIco.sh https://gitlab.renanet.es/icosaedro-public/utils/icosaedro-installer/raw/master/prepareIcosaedroGenericOpenVPNServer.sh?inline=false \
&& chmod +x /tmp/installIco.sh \
&& sudo /tmp/installIco.sh \
&& rm /tmp/installIco.sh
```

TIPS:

Before start, ensure that you have:

- NEXUS user / pass
- GIT user / pass

DO NOT USE ROOT USER, to create an user run the command as root:

```
adduser USERNAME
```

To add the new user into the sudo group run the command as root:

```
usermod -aG sudo USERNAME
```

Updater docker in Server with unattended

The script can be run with the command:

```
wget --no-check-certificate -O /tmp/update_docker_service.sh https://gitlab.renanet.es/icosaedro-public/utils/icosaedro-installer/raw/master/update_docker_service.sh?inline=false \
&& chmod +x /tmp/update_docker_service.sh \
&& sudo bash -c "/tmp/update_docker_service.sh $USER" \
&& rm /tmp/update_docker_service.sh
```