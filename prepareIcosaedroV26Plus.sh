#!/bin/bash

CURRENT_USER=$(whoami)
DEV_REPO_NAME="docker-ico-dev.renanet.es"
PROD_REPO_NAME="docker-ico-prod.renanet.es"
NEXUS_SERVER=${DEV_REPO_NAME}
GITLAB_SERVER="gitlab.renanet.es"
DOCKER_SERVICES_HOME=/srv/DockerServices
ICOSAEDRO_PROJECT_LOCATION="${DOCKER_SERVICES_HOME}/Icosaedro"

if [ "${CURRENT_USER}" != "root" ]; then
  echo "Please run this script as root or with sudo"
  exit
fi

if [ $# -lt 1 ]; then
  echo "Please tell me which user will be the docker manger: $0 userName"
  exit
fi

USE_PRODUCTION_REPOS=""
while [ "${USE_PRODUCTION_REPOS}" != "y" ] && [ "${USE_PRODUCTION_REPOS}" != "n" ]; do
  echo -n "This installation is for production environment? (y -> prod, n -> dev): "
  read -r USE_PRODUCTION_REPOS
done
if [ "${USE_PRODUCTION_REPOS}" == "y" ]; then
  NEXUS_SERVER=${PROD_REPO_NAME}
fi

DOCKER_USER=$1

echo "Updating system..."
apt-get update -y

echo "Preparing base system..."
apt-get -y install net-tools python3 git

echo "Credentials located at: https://pass.renanet.es/app/passwords/view/e855015e-a0ce-425a-a805-c842f1273c58"
echo -n "Please, enter GIT repository user: "
read -r GIT_USER
echo -n "
Please, enter GIT repository pass: "
read -r -s GIT_PASS
echo ""
GIT_PASS_URL=$(python3 -c "import sys, urllib.parse as ul; print(ul.quote_plus('${GIT_PASS}'));")

echo "Preparing system for docker..."
apt-get -y install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update -y

echo "Installing Docker dependencies..."
apt-get -y install ca-certificates curl

echo "Adding Docker's official GPG key:..."
install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
chmod a+r /etc/apt/keyrings/docker.asc

if [ ! -f /etc/apt/sources.list.d/docker.list ]; then
  echo "Adding the Docker repository to APT sources..."
  echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
    $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
    tee /etc/apt/sources.list.d/docker.list > /dev/null
fi

echo "Removing obsolete dependencies..."
for pkg in docker.io docker-doc docker-compose docker-compose-v2 podman-docker containerd runc docker-scan-plugin; do apt-get -y remove $pkg; done

echo "Installing Docker..."
apt-get -y install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

if [ "$(getent passwd "${DOCKER_USER}" | wc -l)" -eq 0 ]; then
  echo "User ${DOCKER_USER} does not exists, please insert user account info"
  adduser "${DOCKER_USER}"
fi

if [ "$(getent group docker | grep -c "${DOCKER_USER}")" -eq 0 ]; then
  echo "Adding user ${DOCKER_USER} to docker group"
  usermod -aG docker "${DOCKER_USER}"
  systemctl restart docker
fi

if [ -f /etc/docker/daemon.json ]; then
  rm /etc/docker/daemon.json
fi

echo "{
  \"log-driver\": \"json-file\",
  \"log-opts\": {
    \"max-size\": \"10m\",
    \"max-file\": \"10\",
    \"compress\": \"true\"
  }
}" >> /etc/docker/daemon.json
systemctl restart docker

NEXUS_CREDENTIALS_LOCATION="https://pass.renanet.es/app/passwords/view/2899105b-e13f-4725-84e4-47788debe111"
if [ "${USE_PRODUCTION_REPOS}" == "y" ]; then
  NEXUS_CREDENTIALS_LOCATION="https://pass.renanet.es/app/passwords/view/ad4d97f1-4421-48aa-9f44-be3af664a001"
fi

DOCKER_USER_HOME=$(getent passwd "${DOCKER_USER}" | cut -d: -f6)
if [ ! -f "${DOCKER_USER_HOME}/.docker/config.json" ] || [ "$(grep -c ${NEXUS_SERVER} "${DOCKER_USER_HOME}/.docker/config.json")" -eq 0 ]; then
  echo "User your LDAP credentials for your PC"
  echo "OR for servers user Nexus credentials located at: ${NEXUS_CREDENTIALS_LOCATION}"
  echo "Please, login with user and password to NEXUS repository: ${NEXUS_SERVER}"
  runuser -l "${DOCKER_USER}" -c "docker login ${NEXUS_SERVER}"
fi

if [ "$(systemctl is-enabled docker)" != "enabled" ]; then
  echo "Enabling docker service on start..."
  systemctl enable docker
fi

echo "Verify docker version"
runuser -l "${DOCKER_USER}" -c "docker -v"

echo "DOCKER Installation Done"

echo "Installing empty Icosaedro latest"
if [ ! -d ${ICOSAEDRO_PROJECT_LOCATION} ]; then
  mkdir -p ${ICOSAEDRO_PROJECT_LOCATION}
  chown "${DOCKER_USER}" ${ICOSAEDRO_PROJECT_LOCATION}
fi

INSTALL_LOCATION=${ICOSAEDRO_PROJECT_LOCATION}/docker-services-$(date +"%Y-%m-%d")
runuser -l "${DOCKER_USER}" -c "export GIT_SSL_NO_VERIFY=1 && git clone https://${GIT_USER}:${GIT_PASS_URL}@${GITLAB_SERVER}/icosaedro/utils/docker-services.git ${INSTALL_LOCATION}"
if [ ! -d "${INSTALL_LOCATION}" ]; then
  echo "Unable to get Icosaedro scripts. Aborting..."
  exit
fi

ICOSAEDRO_LATEST_PROJECT_LOCATION=${ICOSAEDRO_PROJECT_LOCATION}/latest
rm -rf "${ICOSAEDRO_LATEST_PROJECT_LOCATION}"
ln -s "${INSTALL_LOCATION}" "${ICOSAEDRO_LATEST_PROJECT_LOCATION}"

echo "Please re-login to allow user to use docker commands and configure environment at ${ICOSAEDRO_LATEST_PROJECT_LOCATION} before launch Icosaedro system. Have a nice day."
